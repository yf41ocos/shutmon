#include <cstdio>
#include <iostream>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <vector>

using namespace std;

inline bool correctTime(std::vector<unsigned short> &times)
{
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	unsigned short hour = tm.tm_hour;
	unsigned short min = tm.tm_min;
	unsigned short sec = tm.tm_sec;

	unsigned short chour = times[3];
	unsigned short cterm = 0;
	if(times[3] < times[0]) cterm = 24; //correct hours
	chour += cterm;

	//cout << "after= "  << times[0] << ":" << times[1] << ":" << times[2] << endl;
	//cout << "before= "  << times[3] << ":" << times[4] << ":" << times[5] << endl;
	//cout << "now= " << hour << ":" << min << ":" << sec << endl;
	
	unsigned int tafter = times[0]*10000 + times[1]*100 + times[2];
	unsigned int tbefore = (chour)*10000 + times[4]*100 + times[5];
	unsigned int now = (hour+cterm)*10000 + min*100 + sec;
	
	//cout << "vals" << endl << tafter << endl << tbefore << endl << now << endl;

	return (now >= tafter && now <= tbefore);
}

int main (int argc, char **argv)
{

	if(argc != 8){
		cout << "usage: shutdowndaemon idletime onlyafterhour onlyaftermin onlyaftersec onlybeforehour onlybeforemin onlybeforesec"  << endl;
		exit(EXIT_FAILURE);
	}

	unsigned long idletime = atol(argv[1]);
    unsigned short afterhour = atoi(argv[2]);
	unsigned short aftermin = atoi(argv[3]);
	unsigned short aftersec = atoi(argv[4]);
	unsigned short beforehour = atoi(argv[5]);
    unsigned short beforemin = atoi(argv[6]);
    unsigned short beforesec = atoi(argv[7]);

	timeval start;
	gettimeofday(&start, 0);
	unsigned long start_time = start.tv_sec;
	unsigned long time;
	while(true){
		sleep(1);

		FILE *fp;
  		char path[1035];

  		/* Open the command for reading. */
  		fp = popen("netstat -an | grep -E \"\\:22[ \\t]+\" | grep 'ESTABLISHED\\|VERBUNDEN' | wc -l","r");
		if (fp == NULL) {
    		printf("Failed to run command\n" );
    		exit(1);
  		}

  		/* Read the output a line at a time - output it. */
  		int numusers_ssh = 0;
		while (fgets(path, sizeof(path)-1, fp) != NULL) {
    		//printf("%s", path);
			numusers_ssh = atoi(path);
  		}
  		pclose(fp);

		/* Open the command for reading. */
        fp = popen("netstat -an | grep -E \"\\:445[ \\t]+\" | grep 'ESTABLISHED\\|VERBUNDEN' | wc -l","r");
        if (fp == NULL) {
            printf("Failed to run command\n" );
            exit(1);
        }

        /* Read the output a line at a time - output it. */
        int numusers_samba = 0;
        while (fgets(path, sizeof(path)-1, fp) != NULL) {
            //printf("%s", path);
            numusers_samba = atoi(path);
        }
        pclose(fp);

		//cout << numusers_ssh << endl;
		//cout << numusers_samba << endl << endl;
		gettimeofday(&start,0);
		time = start.tv_sec;

        if(numusers_ssh+numusers_samba >= 1){
			start_time = time;
			//cout << "reset" << endl;	
        }

		std::vector<unsigned short> times = {afterhour,aftermin,aftersec,beforehour,beforemin,beforesec};
		if(time - start_time > idletime && correctTime(times)){
			system("shutdown -P now");
			//cout << "shutdown" << endl;
		}
	}

	return 0;

}
